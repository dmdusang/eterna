﻿using UnityEngine;
using System.Collections;

public class HazardController : MonoBehaviour {

    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;




    
    private bool startgamehazards;

    void Start()
    {

        startgamehazards = false;
    }

    void Update()
    {
        if ((Input.touchCount > 0 || Input.anyKeyDown) && startgamehazards == false)
        {
            StartCoroutine(SpawnWaves());
            startgamehazards = true;
        }

    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                //Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, transform.rotation);
                yield return new WaitForSeconds(spawnWait);

            }
            hazardCount = hazardCount + 2;
            yield return new WaitForSeconds(waveWait);
            waveWait = waveWait -1;
            if(waveWait < 0 )
            {
                waveWait = 5;
            }

            if(hazardCount > 50)
            {
                hazardCount = 10;
            }


        }
    }

    
}
