﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LogoFade : MonoBehaviour {

    public Image Logo;
    public float fadeSpeed = 1.5f;
    public float time = 3;



    void Start()
    {
        Logo.canvasRenderer.SetAlpha(1.0f);
        StartCoroutine(SceneChange());

    }
    void Update()
    {

    }

    private IEnumerator SceneChange()
    {
        yield return new WaitForSeconds(time);
        Logo.CrossFadeAlpha(0.0f, fadeSpeed, false);
        yield return new WaitForSeconds(time);
        Application.LoadLevel("Eterna");
    }
}
