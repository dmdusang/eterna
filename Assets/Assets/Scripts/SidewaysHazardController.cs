﻿using UnityEngine;
using System.Collections;

public class SidewaysHazardController : MonoBehaviour {


    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;





    private bool startgamehazards;

    void Start()
    {

        startgamehazards = false;
    }

    void Update()
    {
        if ((Input.touchCount > 0 || Input.anyKeyDown) && startgamehazards == false)
        {
            StartCoroutine(SpawnWaves());
            startgamehazards = true;
        }

    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(spawnValues.x, spawnValues.y, Random.Range(6,16));
                Instantiate(hazard, spawnPosition, transform.rotation);
                yield return new WaitForSeconds(spawnWait);
                
            }
            hazardCount = hazardCount + 1;
            yield return new WaitForSeconds(waveWait);
            waveWait = waveWait -1;
            if (waveWait < 0)
            {
                waveWait = 5;
            }
           
            if(hazardCount > 10)
            {
                hazardCount = 2;
            }


        }
    }
}
