﻿using UnityEngine;
using System.Collections;

public class LoadGame : MonoBehaviour {

    public float speed;
    public float outspeed;
    public float invokenextscene;

    Color skin;
    //Color clothe;
    private bool colorfade;
    //private bool fadein;

    void Start () {

        colorfade = false;
        
        
	}

    void Update()
    {


        if (colorfade == false)
        {
            StartCoroutine(FadeStart());

        }


        else
            StartCoroutine(FadeEnd());

    }
	
    void nextScene()
    {
        Application.LoadLevel("Eterna");
    }

    private IEnumerator FadeStart()
    {
       
        //Color.TryParseHexString("8A8D8D7F", out skin);
         GetComponent<GUITexture>().color = Color.Lerp(GetComponent<GUITexture>().color, Color.gray, Time.deltaTime * speed);
         yield return new WaitForSeconds(6);

         colorfade = true;


    }

    private IEnumerator FadeEnd()
    {
        while (GetComponent<GUITexture>().color != Color.black)
        //Color.TryParseHexString("8A8D8D7F", out skin);
        {
            GetComponent<GUITexture>().color = Color.Lerp(GetComponent<GUITexture>().color, Color.black, Time.deltaTime * speed);
            yield return new WaitForSeconds(1);
        }
        Invoke("nextScene", invokenextscene);
        

    }
}
