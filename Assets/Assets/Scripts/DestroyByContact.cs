﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }
        Instantiate(explosion, transform.position, transform.rotation);
        
        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            //Invoke("restart", 5);
            //gameController.GameOver();
        }
        Destroy(other.gameObject);
        Destroy(gameObject);
    }

    void restart()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}