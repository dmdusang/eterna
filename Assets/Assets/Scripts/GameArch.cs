﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameArch : MonoBehaviour
{


    public Text scoreText;
    public Text titleText;
    public Text pushtoStart;
    // public GUIText restartText;
    // public GUIText gameOverText;
    

    // Audio Sources
    AudioSource audio1;
    AudioSource audio2;
    AudioSource audio3;
    GameObject Player;

    private bool gameOver;
    private bool restart;
    private int score;
    private float timescore;
    private int highscore;
    private bool startgameaudio;
    public float FadeSpeed;
    public float FadeSpeed2;
    public float FadeSpeedIn;
    public float FadeSpeedIn2;
    public float time;
    public float speed;
    private bool divisible10;
    public int time2;
    public float basesoundstart;
    private float timeadd;
    private float timereduce;

    void Awake()
    {
        // Text
        titleText.canvasRenderer.SetAlpha(0.0f);
        pushtoStart.canvasRenderer.SetAlpha(0.0f);
        // Player Check
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    void Start()
    {
        //Speed Variable Initialize
        timeadd = 5.0f;
        timereduce = 40.0f;
        time = 0;
        // Audio
        AudioSource[] audios = GetComponents<AudioSource>();
        audio1 = audios[0];
        audio2 = audios[1];
        audio3 = audios[2];

        startgameaudio = false;
        audio1.Play();

        // Text
        titleText.CrossFadeAlpha(1.0f, FadeSpeedIn, false);
        pushtoStart.CrossFadeAlpha(1.0f, FadeSpeedIn2, false);
 
        
        //Timer Score
        timescore = 0;
        score = 0;
        highscore = PlayerPrefs.GetInt("highscore");
        scoreText.text = "0" + "/" + highscore.ToString();


    }

    void Update()
    {
        // Game Speed Add
        if (startgameaudio)
        {
            if (Time.time > timeadd)
            {
                AddSpeed();
                timeadd += 5.0f;
            }
            if (Time.time > timereduce)
            {
                ReduceSpeed();
                timereduce += 40.0f;
            }

        }
        // Audio On First Touch
        if ((Input.touchCount > 0 || Input.anyKeyDown) && startgameaudio == false)
        {
            StartCoroutine(AudioStart());
            startgameaudio = true;
        }

        //Score Stuff
        highscore = PlayerPrefs.GetInt("highscore");
        if (Player != null && startgameaudio == true)
        {
            timescore += (Time.time)/500;
            score = (int)timescore;
            scoreText.text = score.ToString() + "/" + highscore.ToString();
            StoreHighscore(score);
        }
        else if (Player == null && startgameaudio == true)
        {
            Invoke("restartgame", 5);
        }

    }

    private IEnumerator AudioStart()
    {
        audio1.Stop();
        audio2.Play();
        titleText.CrossFadeAlpha(0.0f, FadeSpeed, false);
        pushtoStart.CrossFadeAlpha(0.0f, FadeSpeed2, false);
        yield return new WaitForSeconds(basesoundstart);
        audio3.Play();
    }



    /*public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }*/

    void StoreHighscore(int newHighscore)
    {
        int oldHighscore = PlayerPrefs.GetInt("highscore", 0);
        if (newHighscore > oldHighscore)
        {
            PlayerPrefs.SetInt("highscore", newHighscore);
        }
        PlayerPrefs.Save();
    }

    void restartgame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public float ReturnSpeed()
    {
        Debug.Log("Current Speed: " +speed);
        return speed;
    }

    public void AddSpeed()
    {
        if(speed < -13)
        {
            speed  = speed -1;
        }
        else
        {
            speed = speed - 2;
        }
        

    }
    public void ReduceSpeed()
    {
        speed = speed + 10;
    }
}