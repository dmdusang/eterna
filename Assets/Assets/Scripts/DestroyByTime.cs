﻿using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour {
    public float lifetime;
    // Use this for initiali
    float time;
	void Start () {
        time = 0;
        GetComponent<AudioSource>().Play();
        Destroy(gameObject, GetComponent<AudioSource>().clip.length);
        
	}
	
    void Update()
    {
        time += Time.deltaTime;

        if (time > lifetime)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<Animator>().enabled = false;
        }
    
    }

}
