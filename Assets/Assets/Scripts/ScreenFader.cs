﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SceneFader : MonoBehaviour
{
    public Image Logo;
    public float fadeSpeed = 1.5f;
    public float time = 3;

    void Awake()
    {
        Logo.canvasRenderer.SetAlpha(1.0f);

    }

    void Start()
    {
        StartCoroutine(SceneChange());
        
    }
    void Update()
    {

    }

    private IEnumerator SceneChange()
    {
        Logo.CrossFadeAlpha(0.0f, fadeSpeed, false);
        yield return new WaitForSeconds(time);
        Invoke("Eterna", 5);
    }
}