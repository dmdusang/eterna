﻿using UnityEngine;
using System.Collections;

public class MoveDiagonaly : MonoBehaviour
{

    public float speed;
    private float basespeed;
    public GameArch timeintruder;
    public float actualspeed;

    void Start()
    {
        timeintruder = FindObjectOfType<GameArch>();
        basespeed = timeintruder.ReturnSpeed();
        actualspeed = speed + basespeed;
    }
    // Update is called once per frame
    void FixedUpdate()
    {

        //transform.position += Vector3.back * speed;
        //GetComponent<Rigidbody>().AddForce(0, 0, speed);
        GetComponent<Rigidbody>().velocity = new Vector3(actualspeed, 0, actualspeed);
    }
}