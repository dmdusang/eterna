﻿using UnityEngine;
using System.Collections;

[System.Serializable]
/*public class boundary
{
    public float xMin, xMax, zMin, zMax;
}*/

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float introspeed;
    public float tilt;
    private bool startgame;
    private bool realgame;
    private bool trigger;
    //public Boundary boundary;

      void Start()
    {
        startgame = false;
        realgame = false;
        trigger = true;
        
    }

    void FixedUpdate()
    {
       /* 

        Vector3 movement = new Vector3(0.0f,0.0f,1.0f);
        GetComponent<Rigidbody>().velocity = movement * speed;

        GetComponent<Rigidbody>().position = new Vector3
        (
            0.0f,
            0.0f,
            0.0f)
        );*/

       /* if ((Input.touchCount > 0 || Input.anyKeyDown) && startgame == true)
        {
            
           // transform.position = Vector3.Lerp(transform.position, startmove, Time.deltaTime* introspeed);
            Vector3 movement = new Vector3(0.0f, 0.0f, 1.0f);
            GetComponent<Rigidbody>().velocity = movement * speed;

            GetComponent<Rigidbody>().position = new Vector3
            (
                0.0f,
                0.0f,
                0.0f
            );

            if (transform.position == startmove)
            {
                startgame = false;
            }
        }*/

        
    }



    void Update()
    {
        if((Input.touchCount > 0 || Input.anyKeyDown) && trigger == true)
        {
            startgame = true;
            trigger = false;
        }
        if (startgame == true)
        {
            Vector3 startmove = new Vector3(0, 0, 0);
            transform.position = Vector3.MoveTowards(transform.position, startmove, Time.deltaTime * introspeed);
            if (transform.position == startmove)
            {
                startgame = false;
                realgame = true;
            }
        }
        else if (Input.touchCount > 0 && realgame == true)
        {
            //Screen is touched
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
            {
                //If the finger is on the screen, object moves to it constantly
                Vector3 touchPosition = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10));

                Vector3 endposition = new Vector3(touchPosition.x, 0, 0);
                if (touchPosition.x < 0)
                {
                    GetComponent<Animator>().SetTrigger("isLeft");
                    transform.position = Vector3.Lerp(transform.position, endposition, Time.deltaTime * speed);
                }
                if (touchPosition.x >0)
                {
                    GetComponent<Animator>().SetTrigger("isRight");
                    transform.position = Vector3.Lerp(transform.position, endposition, Time.deltaTime * speed);
                }

            }
        }
    }
}