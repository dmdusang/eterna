﻿using UnityEngine;
using System.Collections;

public class Instant : MonoBehaviour {
    public GameObject[] stars;
    public Vector3 spawnValues2;
    public int hazardCount;
    // Use this for initialization
    void Awake() {
        for (int i = 0; i < hazardCount; i++)
        {     GameObject star = stars[Random.Range(0, stars.Length)];
              Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues2.x, spawnValues2.x), (spawnValues2.y), Random.Range(-spawnValues2.z, spawnValues2.z));
              Instantiate(star, spawnPosition, transform.rotation);
         }
    }
}
